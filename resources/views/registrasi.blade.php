@extends('layout')

@section('container')
<h3>Form Registrasi</h3>
<form action="/daftar" method="POST">
{{ csrf_field() }}
<form>
  <div class="mb-3">
    <label for="name" class="form-label">Nama</label>
    <input type="text" class="form-control" id="nama_karyawan">
  </div>
  <div class="mb-3">
    <label for="nomor" class="form-label">Nomor</label>
    <input type="text" class="form-control" id="no_karyawan">
  </div>
  <div class="mb-3">
    <label for="telp" class="form-label">No Telepon</label>
    <input type="text" class="form-control" id="no_telp_karyawan">
  </div>
  <div class="mb-3">
    <label for="jabatan" class="form-label">Jabatan</label>
    <input type="text" class="form-control" id="jabatan_karyawan">
  </div>
  <div class="mb-3">
    <label for="divisi" class="form-label">Divisi</label>
    <input type="text" class="form-control" id="divisi_karyawan">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
</form>
@endsection
 