<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegController extends Controller
{
    public function home()
    {
        return view('home');
    }

    public function show()
    {
        return view('registrasi');
    }

    public function login()
    {
        return view('login');
    }

    public function store(Request $req)
    {
        $nama = $req->nama_karyawan;
        $nomor = $req->no_karyawan;
        $telp = $req->no_telp_karyawan;
        $jabatan = $req->jabatan_karyawan;
        $divisi = $req->divisi_karyawan;

        echo "Nama $nama dengan no $nomor";
    }
}
